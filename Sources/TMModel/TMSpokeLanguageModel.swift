//
//  File.swift
//  
//
//  Created by Gerardo Escamilla on 26/11/21.
//

import Foundation

public struct TMSpokeLanguageModel{
    public let iso639_1: String
    public let name: String

    public init(iso630_1: String, name: String){
        self.iso639_1 = iso630_1
        self.name = name
    }
    
    
}
