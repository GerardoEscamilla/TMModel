//
//  File.swift
//  
//
//  Created by Gerardo Escamilla on 26/11/21.
//

import Foundation

public struct TMMovieModel{
    public let adult: Bool
    public let backdropPath: String
    public let genreIds: [Int]
    public let id: Int
    public let originalLanguage: String
    public let originalTitle: String
    public let overview: String
    public let releaseDate: String
    public let posterPath: String
    public let popularity: Double
    public let title: String
    public let video: Bool
    public let voteAverage: Int
    public let voteCount: Int

    public init(
        adult: Bool,
        backdropPath: String,
        genreIds: [Int],
        id: Int,
        originalLanguage: String,
        originalTitle: String,
        overview: String,
        releaseDate: String,
        posterPath: String,
        popularity: Double,
        title: String,
        video: Bool,
        voteAverage: Int,
        voteCount: Int){
        
        self.adult = adult
        self.backdropPath = backdropPath
        self.genreIds = genreIds
        self.id = id
        self.originalLanguage = originalLanguage
        self.originalTitle = originalTitle
        self.overview = overview
        self.releaseDate = releaseDate
        self.posterPath = posterPath
        self.popularity = popularity
        self.title = title
        self.video = video
        self.voteAverage = voteAverage
        self.voteCount = voteCount
    }
}
