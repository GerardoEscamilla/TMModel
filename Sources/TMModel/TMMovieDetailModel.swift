//
//  File.swift
//  
//
//  Created by Gerardo Escamilla on 26/11/21.
//

import Foundation

public struct TMMovieDetailModel{
    public let adult: Bool
    public let backdropPath: String
    public let belongsToCollection: String
    public let budget: Int
    public let genres: [TMGenreModel]
    public let homepage: String
    public let id: Int
    public let imdbId: String
    public let originalLanguage: String
    public let originalTitle: String
    public let overview: String
    public let popularity: Double
    public let posterPath: String
    public let productionCountries:[TMProductionContryModel]
    public let releaseDate: String
    public let revenue: Int
    public let runtime: Int
    public let spokenLanguages: [TMSpokeLanguageModel]
    public let status: String
    public let tagline: String
    public let title: String
    public let video: Bool
    public let voteAverage: Double
    public let voteCount: Int
    
    public init(adult: Bool,
                backdropPath: String,
                belongsToCollection: String,
                budget: Int,
                genres: [TMGenreModel],
                homepage: String,
                id: Int,
                imdbId: String,
                originalLanguage: String,
                originalTitle: String,
                overview: String,
                popularity: Double,
                posterPath: String,
                productionCountries:[TMProductionContryModel],
                releaseDate: String,
                revenue: Int,
                runtime: Int,
                spokenLanguages: [TMSpokeLanguageModel],
                status: String,
                tagline: String,
                title: String,
                video: Bool,
                voteAverage: Double,
                voteCount: Int){
        
        self.adult = adult
        self.backdropPath = backdropPath
        self.belongsToCollection = belongsToCollection
        self.budget = budget
        self.genres = genres
        self.homepage = homepage
        self.id = id
        self.imdbId = imdbId
        self.originalLanguage = originalLanguage
        self.originalTitle = originalTitle
        self.overview = overview
        self.popularity = popularity
        self.posterPath = posterPath
        self.productionCountries = productionCountries
        self.releaseDate = releaseDate
        self.revenue = revenue
        self.runtime = runtime
        self.spokenLanguages = spokenLanguages
        self.status = status
        self.tagline = tagline
        self.title = title
        self.video = video
        self.voteAverage = voteAverage
        self.voteCount = voteCount
    }
}
