//
//  File.swift
//  
//
//  Created by Gerardo Escamilla on 26/11/21.
//

import Foundation

public struct TMProductionCompanyModel{
    public let id: Int
    public let logoPath: String
    public let name: String
    public let originContry: String
    
    public init(id: Int, logoPath: String, name: String, originContry:String){
        self.id = id
        self.logoPath = logoPath
        self.name = name
        self.originContry = originContry
    }
}
