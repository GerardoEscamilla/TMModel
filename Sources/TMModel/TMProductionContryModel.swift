//
//  File.swift
//  
//
//  Created by Gerardo Escamilla on 26/11/21.
//

import Foundation

public struct TMProductionContryModel{
    public let iso3166_1: String
    public let name: String
    
    public init(iso3166_1: String, name: String){
        self.iso3166_1 = iso3166_1
        self.name = name
        
    }
}
