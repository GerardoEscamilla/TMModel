//
//  File.swift
//  
//
//  Created by Gerardo Escamilla on 26/11/21.
//

import Foundation

public struct TMGenreModel{
    
    public let id: Int
    public let name: String
    
    public init(id: Int, name: String){
        self.id = id
        self.name = name
    }
}
